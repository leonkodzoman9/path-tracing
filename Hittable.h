#pragma once

#include "Includes.h"

#include "Vector.h"



struct Ray;
struct Material;



struct HitPoint {

	Vector3d position;
	Vector3d normal;
	double t = FLT_MAX;

	bool isFrontFace = false;

	Material* material = nullptr;

	void setNormalAndFace(const Ray& ray, const Vector3d& normal);
};



template <class Derived>
struct HittableObject {

	Material* material = nullptr;

	bool hit(const Ray& ray, HitPoint& hitPoint) const {
		return ((Derived*)(this))->hit(ray, hitPoint);
	};
};

struct Sphere : public HittableObject<Sphere> {

	Vector3d position;
	double radius = 0;

	bool hit(const Ray& ray, HitPoint& hitPoint) const;
};

struct Triangle : public HittableObject<Sphere> {

	Vector3d p1, p2, p3;

	bool hit(const Ray& ray, HitPoint& hitPoint) const;
};
