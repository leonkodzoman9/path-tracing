#include "Includes.h"

#include "Window.h"
#include "Mouse.h"
#include "Keyboard.h"

#include "Timer.h"
#include "Camera.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include "Scene.h"
#include "Ray.h"
#include "Material.h"
#include "Utility.h"



static const Vector2i wSize(1600, 900);
static constexpr int BlockSize = 10;
static const Vector2i wBlockSize = wSize / BlockSize;

static constexpr int Samples = 1000;
static constexpr int Bounces = 50;

static constexpr int ThreadCount = 8;



enum class DetailLevel {
	LOW,
	HIGH,
};



bool running = false;
std::vector<Vector3i> pixelBlocksToProcess;
std::vector<Vector3f> image;

std::mutex mutex;
std::condition_variable condition;
std::array<std::thread, ThreadCount> threads;



void processPixels(const Scene& scene, Camera& camera) {

	std::random_device device;
	std::mt19937 generator(device());
	std::uniform_real_distribution<float> distribution(0, 1);

	Vector2f pixelSize = Vector2f(1.0f) / Vector2f(wSize);

	while (true) {

		Vector3i pixelBlock(-1);
		Camera cam;
		{
			std::unique_lock lock(mutex);
			condition.wait(lock, [&]() { return pixelBlocksToProcess.size() > 0 || !running; });

			if (!running) {
				break;
			}

			pixelBlock = pixelBlocksToProcess.back();
			cam = camera;
			pixelBlocksToProcess.pop_back();
		}

		if (pixelBlock.x != -1) {

			DetailLevel detailLevel = (DetailLevel)(pixelBlock.z);

			int samples = detailLevel == DetailLevel::LOW ? 10 : Samples;
			int bounces = detailLevel == DetailLevel::LOW ? 5 : Bounces;
			
			int i0 = pixelBlock.y * BlockSize;
			int j0 = pixelBlock.x * BlockSize;

			for (int i = i0; i < i0 + BlockSize; i++) {
				for (int j = j0; j < j0 + BlockSize; j++) {

					Vector2f pixel = Vector2f(j, i) / Vector2f(wSize - 1);

					Vector3f color;
					for (int sample = 0; sample < samples; sample++) {
						Vector2f pos = pixel + (Vector2f(distribution(generator), distribution(generator)) * 0.5f - 0.5f) * pixelSize;
						color += scene.getColor(cam.getRay(pos, wSize), bounces);
					}

					color /= (float)samples;
					color.x = std::clamp(std::powf(color.x, 1.0f / 2.2f), 0.0f, 1.0f);
					color.y = std::clamp(std::powf(color.y, 1.0f / 2.2f), 0.0f, 1.0f);
					color.z = std::clamp(std::powf(color.z, 1.0f / 2.2f), 0.0f, 1.0f);

					image[i * wSize.x + j] = color;
				}
			}
		}
	}
}

void setPixelBlocks(DetailLevel detailLevel) {

	std::vector<int> indices(wBlockSize.x * wBlockSize.y);
	std::iota(indices.begin(), indices.end(), 0);

	std::random_device device;
	std::mt19937 generator(device());
	std::shuffle(indices.begin(), indices.end(), generator);

	std::lock_guard guard(mutex);

	std::fill(image.begin(), image.end(), Vector3f(-1));

	pixelBlocksToProcess.resize(indices.size());
	for (int i = 0; i < indices.size(); i++) {
		pixelBlocksToProcess[i] = Vector3i(indices[i] % wBlockSize.x, indices[i] / wBlockSize.x, (int)detailLevel);
	}

	condition.notify_all();
}



void blurImage(std::vector<Vector3f>& imageToDisplay) {

	std::vector<Vector3f> result(imageToDisplay.size());

	const std::array<float, 9> scales = { 1,2,1,2,4,2,1,2,1 };

	for (int i = 0; i < wSize.y; i++) {
		for (int j = 0; j < wSize.x; j++) {
			if (image[i * wSize.x + j] != Vector3f(-1)) {
				result[i * wSize.x + j] = image[i * wSize.x + j];
			}
			else {

				int count = 0;
				Vector3f sum;

				for (int r = -1; r <= 1; r++) {
					for (int c = -1; c <= 1; c++) {
						int row = i + r;
						int col = j + c;
						int scaleIndex = (r + 1) * 3 + (c + 1);
						if (row >= 0 && row < wSize.y && col >= 0 && col < wSize.x) {
							sum += imageToDisplay[row * wSize.x + col] * scales[scaleIndex];
							count += scales[scaleIndex];
						}
					}
				}

				result[i * wSize.x + j] = sum / (float)count;
			}
		}
	}

	imageToDisplay = result;
}



int main() {

	Window& window = Window::getInstance();
	Mouse& mouse = Mouse::getInstance();
	Keyboard& keyboard = Keyboard::getInstance();

	window.initialize(wSize);

	std::random_device device;
	std::mt19937 generator(6969);
	std::uniform_real_distribution<float> distribution(0, 1);



	Camera camera = Camera(Vector3f(0, 1.5, 20), Vector3f(0, -0.05, -1), 10);

	Scene scene;
	{
		Lambertian* mat = new Lambertian;
		mat->diffuseColor = Vector3f(0.7);
		scene.addQuad(Vector3f(-20, 0, -20), Vector3f(-20, 0, 20), Vector3f(20, 0, 20), Vector3f(20, 0, -20), mat);
	}

	for (int i = 0; i < 100; i++) {

		Material* mat = nullptr;
		if (i % 4 == 0) {
			Lambertian* m = new Lambertian;
			m->diffuseColor = (Vector3f)(Utility::GetPointInUnitSphere()) / 2.0f + 0.5f;
			if (distribution(generator) < 0.3) {
				m->emmisiveColor = m->diffuseColor * 5.0f;
			}
			mat = m;
		}
		else if (i % 4 == 1) {
			Metal* m = new Metal;
			m->diffuseColor = (Vector3f)(Utility::GetPointInUnitSphere()) / 2.0f + 0.5f;
			mat = m;
		}
		else if (i % 4 == 2) {
			FuzzyMetal* m = new FuzzyMetal;
			m->diffuseColor = (Vector3f)(Utility::GetPointInUnitSphere()) / 2.0f + 0.5f;
			m->fuzziness = distribution(generator);
			if (distribution(generator) < 0.3) {
				m->emmisiveColor = m->diffuseColor * 5.0f;
			}
			mat = m;
		}
		else if (i % 4 == 3) {
			Glass* m = new Glass;
			m->diffuseColor = (Vector3f)(Utility::GetPointInUnitSphere()) / 2.0f + 0.5f;
			m->refractionIndex = distribution(generator) + 1;
			mat = m;
		}

		Sphere s;
		s.position = Vector3f(distribution(generator), 0, distribution(generator)) * 20.0f - 10.0f;
		s.radius = distribution(generator) * 0.5 + 0.1;
		s.position.y = s.radius;
		s.material = mat;

		scene.addSphere(s);
	}



	std::vector<Vector3f> imageToDisplay(wSize.x * wSize.y);
	image.resize(imageToDisplay.size());

	setPixelBlocks(DetailLevel::LOW);

	running = true;
	for (int i = 0; i < ThreadCount; i++) {
		threads[i] = std::thread(processPixels, std::ref(scene), std::ref(camera));
	}



	bool isCameraFixed = true;

	auto renderStartTime = std::chrono::steady_clock::now();

	MultiTimer timer(100);
	while (true) {

		timer.update();

		double renderTime = (std::chrono::steady_clock::now() - renderStartTime).count() / 1000000000.0;
		float percentDone = 1.0 - pixelBlocksToProcess.size() / double(wBlockSize.x * wBlockSize.y);

		double timePerPercent = renderTime / percentDone;

		double expectedTime = (1.0 - percentDone) * timePerPercent;

		window.setTitle(std::to_string(timer.getAverageInterval()) + ", " + std::to_string(100 * percentDone) + ", ETA : " + std::to_string(expectedTime) + "s");
		

		glfwPollEvents();

		mouse.update();
		keyboard.update();

		if (!isCameraFixed) {

			bool moved = false;

			if (keyboard.held(KeyCode::W)) { camera.move(camera.front, 0.01f); moved = true; }
			if (keyboard.held(KeyCode::A)) { camera.move(-camera.right, 0.01f); moved = true; }
			if (keyboard.held(KeyCode::S)) { camera.move(-camera.front, 0.01f); moved = true; }
			if (keyboard.held(KeyCode::D)) { camera.move(camera.right, 0.01f); moved = true; }
			if (keyboard.held(KeyCode::SPACE)) { camera.move(Vector3f::Up(), 0.01f); moved = true; }
			if (keyboard.held(KeyCode::LEFT_SHIFT)) { camera.move(-Vector3f::Up(), 0.01f); moved = true; }

			if (keyboard.held(KeyCode::UP)) {
				if (camera.fov > 1) {
					camera.fov -= 0.1;
				}
				moved = true;
			}
			if (keyboard.held(KeyCode::DOWN)) {
				camera.fov += 0.1;
				moved = true;
			}

			if (mouse.deltaPosition().magnitude2() > 0) {
				camera.rotate(mouse.deltaPosition(), 0.1, true);
				moved = true;
			}

			if (moved) {
				setPixelBlocks(DetailLevel::LOW);
				std::fill(imageToDisplay.begin(), imageToDisplay.end(), Vector3f(0));
			}
		}

		if (keyboard.held(KeyCode::LEFT_CONTROL) && keyboard.pressed(KeyCode::Q)) {
			std::lock_guard guard(mutex);
			pixelBlocksToProcess.clear();
			running = false;
			break;
		}

		if (keyboard.pressed(KeyCode::TAB)) {
			isCameraFixed = !isCameraFixed;
			window.setMouseState(isCameraFixed ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);

			if (isCameraFixed) {
				setPixelBlocks(DetailLevel::HIGH);
				std::fill(imageToDisplay.begin(), imageToDisplay.end(), Vector3f(0));
				renderStartTime = std::chrono::steady_clock::now();
			}
		}

		glClear(GL_COLOR_BUFFER_BIT);

		blurImage(imageToDisplay);
		glDrawPixels(wSize.x, wSize.y, GL_RGB, GL_FLOAT, imageToDisplay.data());

		glfwSwapBuffers(window.getWindow());
	} 

	for (int i = 0; i < ThreadCount; i++) {
		threads[i].join();
	}

	return 0;
}


