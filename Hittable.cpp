#include "Hittable.h"

#include "Ray.h"



void HitPoint::setNormalAndFace(const Ray& ray, const Vector3d& normal) {

	this->isFrontFace = Math::Dot(ray.direction, normal) < 0.0;
	this->normal = Math::Normalize(this->isFrontFace ? normal : -normal);
}



bool Sphere::hit(const Ray& ray, HitPoint& hitPoint) const {

	double a = Math::Dot(ray.direction, ray.direction);
	double bHalf = Math::Dot(ray.direction, ray.origin - this->position);
	double c = Math::Dot(ray.origin - this->position, ray.origin - this->position) - this->radius * this->radius;

	double d = bHalf * bHalf - a * c;

	if (d < 0) { return false; }

	double t1 = (-bHalf + std::sqrt(d)) / a;
	double t2 = (-bHalf - std::sqrt(d)) / a;

	double t = FLT_MAX;
	if (t1 > 0.0001 && t1 < hitPoint.t) { t = t1; }
	if (t2 > 0.0001 && t2 < hitPoint.t && t2 < t) { t = t2; }

	if (t == FLT_MAX) { return false; }

	hitPoint.position = ray.origin + ray.direction * t;
	hitPoint.setNormalAndFace(ray, hitPoint.position - this->position);
	hitPoint.material = this->material;
	hitPoint.t = t;

	return true;
}

bool Triangle::hit(const Ray& ray, HitPoint& hitPoint) const {

	Vector3d edge1 = this->p2 - this->p1;
	Vector3d edge2 = this->p3 - this->p1;

	Vector3d h = Math::Cross(ray.direction, edge2);
	double a = Math::Dot(edge1, h);

	if (std::abs(a) < 0.0001) { return false; }

	double f = 1.0 / a;
	Vector3d s = ray.origin - this->p1;
	double u = f * Math::Dot(s, h);

	if (u < 0.0 || u > 1.0) { return false; }

	Vector3d q = Math::Cross(s, edge1);
	double v = f * Math::Dot(ray.direction, q);

	if (v < 0.0 || u + v > 1.0) { return false; }

	double t = f * Math::Dot(edge2, q);

	if (t > 0.0001 && t < hitPoint.t) {

		hitPoint.position = ray.origin + ray.direction * t;
		hitPoint.material = this->material;
		hitPoint.isFrontFace = true;
		hitPoint.t = t;

		Vector3d vert = Math::Cross(edge1, edge2);
		double dp = Math::Dot(vert, ray.direction);

		hitPoint.normal = dp < 0 ? vert : -vert;

		return true;
	}

	return false;
}


