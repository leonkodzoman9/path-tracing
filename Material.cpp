#include "Material.h"

#include "Hittable.h"
#include "Utility.h"



Ray Lambertian::scatter(const Ray& incoming, const HitPoint& hitPoint) const {

	Ray ray{};
	ray.origin = hitPoint.position;
	ray.direction = hitPoint.normal + Utility::GetPointInUnitSphere();

	return ray;
}

Ray Metal::scatter(const Ray& incoming, const HitPoint& hitPoint) const {

	Ray ray{};
	ray.origin = hitPoint.position;
	ray.direction = Utility::Reflect(incoming.direction, hitPoint.normal);

	return ray;
}

Ray FuzzyMetal::scatter(const Ray& incoming, const HitPoint& hitPoint) const {

	Ray ray{};
	ray.origin = hitPoint.position;
	ray.direction = Utility::Reflect(incoming.direction, hitPoint.normal) + this->fuzziness * Utility::GetPointInUnitSphere();

	return ray;
}

Ray Glass::scatter(const Ray& incoming, const HitPoint& hitPoint) const {

	static std::mt19937 generator;
	std::uniform_real_distribution<double> distribution(0, 1);

	Ray ray{};
	ray.origin = hitPoint.position;

	double refractionRatio = hitPoint.isFrontFace ? 1.0 / this->refractionIndex : this->refractionIndex;

	double cosAngle = std::min(Math::Dot(-incoming.direction, hitPoint.normal), 1.0);
	double sinAngle = std::sqrt(1.0 - cosAngle * cosAngle);
	double reflectance = Utility::GetReflectance(cosAngle, refractionRatio);

	bool canReflect = refractionRatio * sinAngle > 1.0 || reflectance > distribution(generator);

	ray.direction = canReflect ? Utility::Reflect(incoming.direction, hitPoint.normal) : Utility::Refract(incoming.direction, hitPoint.normal, cosAngle, refractionRatio);

	return ray;
}
