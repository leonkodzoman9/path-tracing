#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <fstream>

#include <cmath>
#include <ctime>

#include <chrono>
#include <thread>
#include <algorithm>
#include <random>
#include <numeric>
#include <numbers>
#include <execution>

#include <string>
#include <sstream>

#include <array>
#include <vector>

#include <type_traits>