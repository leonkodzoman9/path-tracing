#include "Scene.h"

#include "Ray.h"
#include "Material.h"



Vector3f Scene::getColor(const Ray& ray, int bounces) const {

	Vector3f finalColor(0);
	Vector3f multiplier(1);

	Ray sourceRay = ray;

	for (int i = 0; i < bounces; i++) {

		HitPoint hitPoint;
		bool hit = this->getHitPoint(sourceRay, hitPoint);

		if (hit) {

			finalColor += multiplier * hitPoint.material->emmisiveColor;
			multiplier *= hitPoint.material->diffuseColor;

			sourceRay = hitPoint.material->scatter(sourceRay, hitPoint);
		}
		else {

			float percentage = ray.direction.y * 0.5 + 0.5;
			Vector3f defaultColor = Vector3f(1 - percentage * 0.4, 1 - percentage * 0.2, 1);

			return finalColor + multiplier * defaultColor;
		}
	}

	return finalColor;
}

void Scene::addSphere(Sphere sphere) {

	this->spheres.push_back(sphere);
}
void Scene::addTriangle(Triangle triangle) {

	this->triangles.push_back(triangle);
}
void Scene::addQuad(Vector3f p1, Vector3f p2, Vector3f p3, Vector3f p4, Material* material) {

	Triangle t1, t2;
	t1.p1 = p1;
	t1.p2 = p2;
	t1.p3 = p3;
	t1.material = material;
	t2.p1 = p1;
	t2.p2 = p3;
	t2.p3 = p4;
	t2.material = material;

	this->triangles.push_back(t1);
	this->triangles.push_back(t2);
}

bool Scene::getHitPoint(const Ray& ray, HitPoint& hitPoint) const {

	bool hit = false;
	for (int i = 0; i < this->spheres.size(); i++) {
		hit += this->spheres[i].hit(ray, hitPoint);
	}
	for (int i = 0; i < this->triangles.size(); i++) {
		hit += this->triangles[i].hit(ray, hitPoint);
	}

	return hit;
}
