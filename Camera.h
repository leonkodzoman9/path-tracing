#pragma once

#include "Includes.h"

#include "Vector.h"



struct Ray;



class Camera {

public:

	Vector3f position;
	Vector3f front;
	Vector3f up;
	Vector3f right;

	float fov = 0;

	float yaw = 0;
	float pitch = 0;

	Camera() = default;
	Camera(Vector3f position, Vector3f front, float fov = 60);

	void move(Vector3f direction, float frametime);
	void rotate(Vector2f delta, float timestep, bool constrainPitch);

	Ray getRay(Vector2f pixel, Vector2i wSize) const;

private:

	void updateVectors();
};
