#pragma once

#include "Includes.h"



enum class KeyCode {

	A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
	NUM_0, NUM_1, NUM_2, NUM_3, NUM_4, NUM_5, NUM_6, NUM_7, NUM_8, NUM_9,

	LEFT_SHIFT, LEFT_CONTROL, LEFT_ALT,
	RIGHT_SHIFT, RIGHT_CONTROL, RIGHT_ALT,

	SPACE, COMMA, MINUS, PERIOD, SLASH, SEMICOLON, EQUAL,
	ESCAPE, ENTER, TAB, BACKSPACE, RIGHT, LEFT, UP, DOWN,

	KP_NUM_0, KP_NUM_1, KP_NUM_2, KP_NUM_3, KP_NUM_4, KP_NUM_5, KP_NUM_6, KP_NUM_7, KP_NUM_8, KP_NUM_9,
	KP_DIVIDE, KP_MULTIPLY, KP_ADD, KP_SUBTRACT,

	KEY_LAST = KP_SUBTRACT,
	KEY_FIRST = A
};



uint32_t getKeyValue(KeyCode keyCode);
char getKey(KeyCode keyCode);



class Keyboard {

private:

	std::array<bool, (int)KeyCode::KEY_LAST + 1> currentState;
	std::array<bool, (int)KeyCode::KEY_LAST + 1> previousState;

	std::array<std::pair<float, float>, (int)KeyCode::KEY_LAST + 1> pressTransitionCounter;
	std::array<std::pair<float, float>, (int)KeyCode::KEY_LAST + 1> releaseTransitionCounter;

	float currentTime;

public:

	static Keyboard& getInstance();

	void update();
	void reset();

	bool held(KeyCode key) const;
	bool pressed(KeyCode key) const;
	bool released(KeyCode key) const;

	float heldFor(KeyCode key) const;
	float releasedFor(KeyCode key) const;

	float pressDelta(KeyCode key) const;
	float releaseDelta(KeyCode key) const;

private:

	Keyboard();
	Keyboard(const Keyboard& rhs) = delete;
	Keyboard& operator=(const Keyboard& rhs) = delete;
	Keyboard(Keyboard&& rhs) noexcept = delete;
	Keyboard& operator=(Keyboard&& rhs) noexcept = delete;

	void updatePressTransitionCounter(KeyCode key, float currentTime);
	void updateReleaseTransitionCounter(KeyCode key, float currentTime);
};


