#pragma once

#include "Includes.h"

#include "Vector.h"



struct Ray {

	Vector3d origin;
	Vector3d direction;
};
