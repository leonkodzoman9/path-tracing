#pragma once

#include "Includes.h"

#include "Vector.h"



namespace Utility {

	Vector3d Reflect(Vector3d vector, Vector3d normal);

	Vector3d Refract(Vector3d vector, Vector3d normal, double cosAngle, double refractionRatio);

	Vector3d GetPointInUnitSphere();

	double GetReflectance(double cosAngle, double refractionIndex);
}

