#include "Keyboard.h"

#include "Window.h"



uint32_t getKeyValue(KeyCode keyCode) {

	switch (keyCode) {
	case KeyCode::A:			return GLFW_KEY_A;
	case KeyCode::B:			return GLFW_KEY_B;
	case KeyCode::C:			return GLFW_KEY_C;
	case KeyCode::D:			return GLFW_KEY_D;
	case KeyCode::E:			return GLFW_KEY_E;
	case KeyCode::F:			return GLFW_KEY_F;
	case KeyCode::G:			return GLFW_KEY_G;
	case KeyCode::H:			return GLFW_KEY_H;
	case KeyCode::I:			return GLFW_KEY_I;
	case KeyCode::J:			return GLFW_KEY_J;
	case KeyCode::K:			return GLFW_KEY_K;
	case KeyCode::L:			return GLFW_KEY_L;
	case KeyCode::M:			return GLFW_KEY_M;
	case KeyCode::N:			return GLFW_KEY_N;
	case KeyCode::O:			return GLFW_KEY_O;
	case KeyCode::P:			return GLFW_KEY_P;
	case KeyCode::Q:			return GLFW_KEY_Q;
	case KeyCode::R:			return GLFW_KEY_R;
	case KeyCode::S:			return GLFW_KEY_S;
	case KeyCode::T:			return GLFW_KEY_T;
	case KeyCode::U:			return GLFW_KEY_U;
	case KeyCode::V:			return GLFW_KEY_V;
	case KeyCode::W:			return GLFW_KEY_W;
	case KeyCode::X:			return GLFW_KEY_X;
	case KeyCode::Y:			return GLFW_KEY_Y;
	case KeyCode::Z:			return GLFW_KEY_Z;
	case KeyCode::NUM_0:		return GLFW_KEY_0;
	case KeyCode::NUM_1:		return GLFW_KEY_1;
	case KeyCode::NUM_2:		return GLFW_KEY_2;
	case KeyCode::NUM_3:		return GLFW_KEY_3;
	case KeyCode::NUM_4:		return GLFW_KEY_4;
	case KeyCode::NUM_5:		return GLFW_KEY_5;
	case KeyCode::NUM_6:		return GLFW_KEY_6;
	case KeyCode::NUM_7:		return GLFW_KEY_7;
	case KeyCode::NUM_8:		return GLFW_KEY_8;
	case KeyCode::NUM_9:		return GLFW_KEY_9;
	case KeyCode::LEFT_SHIFT:	return GLFW_KEY_LEFT_SHIFT;
	case KeyCode::LEFT_CONTROL:	return GLFW_KEY_LEFT_CONTROL;
	case KeyCode::LEFT_ALT:		return GLFW_KEY_LEFT_ALT;
	case KeyCode::RIGHT_SHIFT:	return GLFW_KEY_RIGHT_SHIFT;
	case KeyCode::RIGHT_CONTROL:return GLFW_KEY_RIGHT_CONTROL;
	case KeyCode::RIGHT_ALT:	return GLFW_KEY_RIGHT_ALT;
	case KeyCode::SPACE:		return GLFW_KEY_SPACE;
	case KeyCode::COMMA:		return GLFW_KEY_COMMA;
	case KeyCode::MINUS:		return GLFW_KEY_MINUS;
	case KeyCode::PERIOD:		return GLFW_KEY_PERIOD;
	case KeyCode::SLASH:		return GLFW_KEY_SLASH;
	case KeyCode::SEMICOLON:	return GLFW_KEY_SEMICOLON;
	case KeyCode::EQUAL:		return GLFW_KEY_EQUAL;
	case KeyCode::ESCAPE:		return GLFW_KEY_ESCAPE;
	case KeyCode::ENTER:		return GLFW_KEY_ENTER;
	case KeyCode::TAB:			return GLFW_KEY_TAB;
	case KeyCode::BACKSPACE:	return GLFW_KEY_BACKSPACE;
	case KeyCode::RIGHT:		return GLFW_KEY_RIGHT;
	case KeyCode::LEFT:			return GLFW_KEY_LEFT;
	case KeyCode::UP:			return GLFW_KEY_UP;
	case KeyCode::DOWN:			return GLFW_KEY_DOWN;
	case KeyCode::KP_NUM_0:		return GLFW_KEY_KP_0;
	case KeyCode::KP_NUM_1:		return GLFW_KEY_KP_1;
	case KeyCode::KP_NUM_2:		return GLFW_KEY_KP_2;
	case KeyCode::KP_NUM_3:		return GLFW_KEY_KP_3;
	case KeyCode::KP_NUM_4:		return GLFW_KEY_KP_4;
	case KeyCode::KP_NUM_5:		return GLFW_KEY_KP_5;
	case KeyCode::KP_NUM_6:		return GLFW_KEY_KP_6;
	case KeyCode::KP_NUM_7:		return GLFW_KEY_KP_7;
	case KeyCode::KP_NUM_8:		return GLFW_KEY_KP_8;
	case KeyCode::KP_NUM_9:		return GLFW_KEY_KP_9;
	case KeyCode::KP_DIVIDE:	return GLFW_KEY_KP_DIVIDE;
	case KeyCode::KP_MULTIPLY:	return GLFW_KEY_KP_MULTIPLY;
	case KeyCode::KP_ADD:		return GLFW_KEY_KP_ADD;
	case KeyCode::KP_SUBTRACT:	return GLFW_KEY_KP_SUBTRACT;
	}

	return 0;
}

char getKey(KeyCode keyCode) {

	switch (keyCode) {
	case KeyCode::A:			return 'A';
	case KeyCode::B:			return 'B';
	case KeyCode::C:			return 'C';
	case KeyCode::D:			return 'D';
	case KeyCode::E:			return 'E';
	case KeyCode::F:			return 'F';
	case KeyCode::G:			return 'G';
	case KeyCode::H:			return 'H';
	case KeyCode::I:			return 'I';
	case KeyCode::J:			return 'J';
	case KeyCode::K:			return 'K';
	case KeyCode::L:			return 'L';
	case KeyCode::M:			return 'M';
	case KeyCode::N:			return 'N';
	case KeyCode::O:			return 'O';
	case KeyCode::P:			return 'P';
	case KeyCode::Q:			return 'Q';
	case KeyCode::R:			return 'R';
	case KeyCode::S:			return 'S';
	case KeyCode::T:			return 'T';
	case KeyCode::U:			return 'U';
	case KeyCode::V:			return 'V';
	case KeyCode::W:			return 'W';
	case KeyCode::X:			return 'X';
	case KeyCode::Y:			return 'Z';
	case KeyCode::Z:			return 'Y';

	case KeyCode::NUM_0:		return '0';
	case KeyCode::NUM_1:		return '1';
	case KeyCode::NUM_2:		return '2';
	case KeyCode::NUM_3:		return '3';
	case KeyCode::NUM_4:		return '4';
	case KeyCode::NUM_5:		return '5';
	case KeyCode::NUM_6:		return '6';
	case KeyCode::NUM_7:		return '7';
	case KeyCode::NUM_8:		return '8';
	case KeyCode::NUM_9:		return '9';

	case KeyCode::SPACE:		return ' ';
	case KeyCode::COMMA:		return ',';
	case KeyCode::MINUS:		return '-';
	case KeyCode::PERIOD:		return '.';
	case KeyCode::SLASH:		return '/';
	case KeyCode::SEMICOLON:	return ';';
	case KeyCode::EQUAL:		return '=';

	case KeyCode::KP_NUM_0:		return '0';
	case KeyCode::KP_NUM_1:		return '1';
	case KeyCode::KP_NUM_2:		return '2';
	case KeyCode::KP_NUM_3:		return '3';
	case KeyCode::KP_NUM_4:		return '4';
	case KeyCode::KP_NUM_5:		return '5';
	case KeyCode::KP_NUM_6:		return '6';
	case KeyCode::KP_NUM_7:		return '7';
	case KeyCode::KP_NUM_8:		return '8';
	case KeyCode::KP_NUM_9:		return '9';

	case KeyCode::KP_DIVIDE:	return '/';
	case KeyCode::KP_MULTIPLY:	return '*';
	case KeyCode::KP_ADD:		return '+';
	case KeyCode::KP_SUBTRACT:	return '-';
	}

	return 0;
}



Keyboard& Keyboard::getInstance() {

	// A static instance becomes a singleton, first time initialized, other times just returned
	static Keyboard instance;

	return instance;
}

void Keyboard::update() {

	GLFWwindow* window = Window::getInstance().getWindow();

	this->currentTime = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now().time_since_epoch()).count() / 1'000'000.0f;

	for (int i = (int)KeyCode::KEY_FIRST; i <= (int)KeyCode::KEY_LAST; i++) {

		this->previousState[i] = this->currentState[i];
		this->currentState[i] = glfwGetKey(window, getKeyValue(KeyCode(i))) == GLFW_PRESS;

		bool currentState = this->currentState[i];
		bool previousState = this->previousState[i];

		if (!previousState && currentState) {
			this->updatePressTransitionCounter(KeyCode(i), currentTime);
		}

		if (previousState && !currentState) {
			this->updateReleaseTransitionCounter(KeyCode(i), currentTime);
		}
	}
}
void Keyboard::reset() {

	std::fill(this->currentState.begin(), this->currentState.end(), 0);
	std::fill(this->previousState.begin(), this->previousState.end(), 0);

	this->currentTime = 0;
	std::fill(this->pressTransitionCounter.begin(), this->pressTransitionCounter.end(), std::pair<float, float>(0, 0));
	std::fill(this->releaseTransitionCounter.begin(), this->releaseTransitionCounter.end(), std::pair<float, float>(0, 0));
}

bool Keyboard::held(KeyCode key) const {

	return this->currentState[(int)key];
}
bool Keyboard::pressed(KeyCode key) const {

	return this->currentState[(int)key] && !this->previousState[(int)key];
}
bool Keyboard::released(KeyCode key) const {

	return !this->currentState[(int)key] && this->previousState[(int)key];
}

float Keyboard::heldFor(KeyCode key) const {

	return this->held(key) ? this->currentTime - this->pressTransitionCounter[(int)key].second : 0;
}
float Keyboard::releasedFor(KeyCode key) const {

	return !this->held(key) ? this->currentTime - this->releaseTransitionCounter[(int)key].second : 0;
}

float Keyboard::pressDelta(KeyCode key) const {

	return this->pressTransitionCounter[(int)key].second - this->pressTransitionCounter[(int)key].first;
}
float Keyboard::releaseDelta(KeyCode key) const {

	return this->releaseTransitionCounter[(int)key].second - this->releaseTransitionCounter[(int)key].first;
}



Keyboard::Keyboard() {

	this->reset();
};

void Keyboard::updatePressTransitionCounter(KeyCode key, float currentTime) {

	this->pressTransitionCounter[(int)key].first = this->pressTransitionCounter[(int)key].second;
	this->pressTransitionCounter[(int)key].second = currentTime;
}
void Keyboard::updateReleaseTransitionCounter(KeyCode key, float currentTime) {

	this->releaseTransitionCounter[(int)key].first = this->releaseTransitionCounter[(int)key].second;
	this->releaseTransitionCounter[(int)key].second = currentTime;
}
