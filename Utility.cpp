#include "Utility.h"



Vector3d Utility::Reflect(Vector3d vector, Vector3d normal) {

	return vector - normal * 2.0 * Math::Dot(vector, normal);
}

Vector3d Utility::Refract(Vector3d vector, Vector3d normal, double cosAngle, double refractionRatio) {

	Vector3d perpendicular = refractionRatio * (vector + cosAngle * normal);
	Vector3d parallel = -std::sqrt(std::abs(1.0 - perpendicular.magnitude2())) * normal;

	return perpendicular + parallel;
}

Vector3d Utility::GetPointInUnitSphere() {

	static std::mt19937 generator;
	std::uniform_real_distribution<double> distribution(0, 1);

	double u = distribution(generator);
	double v = distribution(generator);

	double theta = distribution(generator) * std::numbers::pi * 2;
	double phi = std::acos(2.0 * distribution(generator) - 1.0);

	return std::cbrt(distribution(generator)) * Vector3d(std::sin(phi) * std::cos(theta), std::sin(phi) * std::sin(theta), std::cos(phi));
}

double Utility::GetReflectance(double cosAngle, double refractionIndex) {
	
	float r0 = (1.0 - refractionIndex) / (1.0 + refractionIndex);
	r0 *= r0;

	return r0 + (1.0 - r0) * std::pow(1.0 - cosAngle, 5.0);
}
