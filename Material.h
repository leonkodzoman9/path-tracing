#pragma once

#include "Includes.h"

#include "Vector.h"

#include "Ray.h"



struct HitPoint;



struct Material {

	Vector3f diffuseColor;
	Vector3f emmisiveColor;

	virtual Ray scatter(const Ray& incoming, const HitPoint& hitPoint) const = 0;
};



struct Lambertian : public Material {

	Ray scatter(const Ray& incoming, const HitPoint& hitPoint) const;
};
struct Metal : public Material {

	Ray scatter(const Ray& incoming, const HitPoint& hitPoint) const;
};
struct FuzzyMetal : public Material {

	double fuzziness = 0.1;

	Ray scatter(const Ray& incoming, const HitPoint& hitPoint) const;
};
struct Glass : public Material {

	double refractionIndex = 1.5;

	Ray scatter(const Ray& incoming, const HitPoint& hitPoint) const;
};