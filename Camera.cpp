#include "Camera.h"

#include "Ray.h"



Camera::Camera(Vector3f position, Vector3f front, float fov) {

	this->position = position;
	this->fov = fov;
	this->yaw = front.yaw() * (180.0f / std::numbers::pi_v<float>);
	this->pitch = front.pitch() * (180.0f / std::numbers::pi_v<float>);

	this->updateVectors();
}

void Camera::move(Vector3f direction, float frametime) {

	this->position += direction * frametime;
}
void Camera::rotate(Vector2f delta, float timestep, bool constrainPitch) {

	this->yaw += delta.x * timestep;
	this->pitch -= delta.y * timestep;

	if (constrainPitch) {
		if (this->pitch > 89.0f) {
			this->pitch = 89.0f;
		}
		if (this->pitch < -89.0f) {
			this->pitch = -89.0f;
		}
	}

	this->updateVectors();
}

Ray Camera::getRay(Vector2f pixel, Vector2i wSize) const {

	float aspectRatio = (float)wSize.x / (float)wSize.y;

	float radian = 180.0f / std::numbers::pi_v<float>;
	float tanFov = std::tan(this->fov / radian * 0.5f) * 2;

	Vector3f up = this->up * tanFov;
	Vector3f right = this->right * tanFov * aspectRatio;

	Ray ray;
	ray.origin = this->position;
	ray.direction = this->front + right * (pixel.x - 0.5f) + up * (pixel.y - 0.5f);

	return ray;
}



void Camera::updateVectors() {

	float sinPitch = std::sin(this->pitch / (180.0f / std::numbers::pi_v<float>));
	float cosPitch = std::cos(this->pitch / (180.0f / std::numbers::pi_v<float>));

	float sinYaw = std::sin(this->yaw / (180.0f / std::numbers::pi_v<float>));
	float cosYaw = std::cos(this->yaw / (180.0f / std::numbers::pi_v<float>));

	this->front = Vector3f(cosPitch * cosYaw, sinPitch, cosPitch * sinYaw);
	this->right = Math::Normalize(Math::Cross(this->front, Vector3f(0, 1, 0)));
	this->up = Math::Normalize(Math::Cross(this->right, this->front));
}

