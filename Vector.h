#pragma once

#include <iostream>
#include <array>
#include <cmath>

#include <immintrin.h>


static constexpr bool Optimize = true;

template <class Type>
struct OptimizeFloat {
	static constexpr bool value = std::is_same<Type, float>::value && Optimize;
};
template <class Type>
struct OptimizeAny {
	static constexpr bool value = OptimizeFloat<Type>::value;
};



// Default vector members
template <class DataType, int Size>
struct VectorMembers {

	std::array<DataType, Size> data;
};

// Members if size is 2
template <class DataType>
struct VectorMembers<DataType, 2> {

	union {
		std::array<DataType, 2> data;
		struct {
			DataType x, y;
		};
	};
};

// Members if size is 3
template <class DataType>
struct VectorMembers<DataType, 3> {

	union {
		std::array<DataType, 3> data;
		struct {
			DataType x, y, z;
		};
	};
};

// Members if size is 4
template <class DataType>
struct VectorMembers<DataType, 4> {

	union {
		std::array<DataType, 4> data;
		struct {
			DataType x, y, z, w;
		};
	};
};



template <class DataType, int Size>
class Vector : public VectorMembers<DataType, Size> {

public:

	// Members are inherited at compile time

	// Constructors

	// Initialize vector to all zeroes
	Vector() {

		for (int i = 0; i < Size; i++) {
			this->data[i] = (DataType)0;
		}
	}

	// Initialize vector to a single value, unoptimized case
	template <class Empty = void> requires (!OptimizeAny<DataType>::value)
		explicit Vector(DataType value) {

		for (int i = 0; i < Size; i++) {
			this->data[i] = value;
		}
	}
	// Initialize vector to a single value, float case
	template <class Empty = void> requires (OptimizeFloat<DataType>::value)
		explicit Vector(DataType value) {

		__m128 _value = _mm_set1_ps(value);

		for (int i = 0, j = 0; i < Size / 4; i++, j += 4) {
			_mm_storeu_ps(&this->data[j], _value);
		}
		for (int i = (Size / 4) * 4; i < Size; i++) {
			this->data[i] = value;
		}
	}

	// Initialize vector from Size values
	template <class... ValueType> requires (sizeof...(ValueType) == Size)
		explicit Vector(ValueType... values) {

		int i = 0;
		([&](auto& value) { this->data[i++] = (DataType)value; } (values), ...);
	}

	// Initialize vector from a vector smaller then the current one with the remaining values provided individually
	template <int Size2, class... ValueType> requires ((sizeof...(ValueType) + Size2) == Size && Size2 < Size)
		explicit Vector(Vector<DataType, Size2> vector, ValueType... values) {

		for (int i = 0; i < Size2; i++) {
			this->data[i] = vector.data[i];
		}

		int i = Size2;
		([&](auto& value) { this->data[i++] = (DataType)value; } (values), ...);
	}

	// Initialize a vector from one of different size and same datatype
	template <int Size2>
	explicit Vector(const Vector<DataType, Size2>& vector) {

		int i = 0;
		for (; i < std::min(Size, Size2); i++) {
			this->data[i] = vector.data[i];
		}
		for (; i < Size; i++) {
			this->data[i] = (DataType)0;
		}
	}

	// Initialize a vector from one of same size but different datatype
	template <class DataType2>
	Vector(const Vector<DataType2, Size>& vector) {

		for (int i = 0; i < Size; i++) {
			this->data[i] = (DataType)vector.data[i];
		}
	}

	// Access element at index 
	DataType& operator[](int index) {

		return this->data[index];
	}

	template <class Empty = void> requires (OptimizeFloat<DataType>::value)
		Vector<DataType, Size>& operator=(const Vector<DataType, Size>& vector) {

#ifdef __AVX2__

		constexpr int offset8f = 0;
		constexpr int count8f = (Size - offset8f) / 8;
		constexpr int offset4f = count8f * 8;
		constexpr int count4f = (Size - offset4f) / 4;
		constexpr int offset1f = offset4f + count4f * 4;
		constexpr int count1f = (Size - offset1f) / 1;

		for (int i = 0; i < count8f; i++) {
			const int index = offset8f + i * 8;
			_mm256_storeu_ps(&this->data[index], _mm256_loadu_ps(&vector.data[index]));
		}
		for (int i = 0; i < count4f; i++) {
			const int index = offset4f + i * 4;
			_mm_storeu_ps(&this->data[index], _mm_loadu_ps(&vector.data[index]));
		}
		for (int i = 0; i < count1f; i++) {
			const int index = offset1f + i * 1;
			this->data[index] = vector.data[index];
		}
#else

		constexpr int offset4f = 0;
		constexpr int count4f = (Size - offset4f) / 4;
		constexpr int offset1f = offset4f + count4f * 4;
		constexpr int count1f = (Size - offset1f) / 1;

		for (int i = 0; i < count4f; i++) {
			const int index = offset4f + i * 4;
			_mm_storeu_ps(&this->data[index], _mm_loadu_ps(&vector.data[index]));
		}
		for (int i = 0; i < count1f; i++) {
			const int index = offset1f + i * 1;
			this->data[index] = vector.data[index];
		}
#endif
		return *this;
	}

	// Assign vector from one of same size but any datatype
	template <class DataType2> requires (!OptimizeAny<DataType>::value || !OptimizeAny<DataType2>::value)
		Vector<DataType, Size>& operator=(const Vector<DataType2, Size>& vector) {

		for (int i = 0; i < Size; i++) {
			this->data[i] = (DataType)vector.data[i];
		}

		return *this;
	}

	// Magnitude of the vector
	DataType magnitude() const {

		return (DataType)std::sqrt(this->magnitude2());
	}
	// Squared magnitude of vector
	DataType magnitude2() const {

		DataType sum = 0;
		for (int i = 0; i < Size; i++) {
			sum += this->data[i] * this->data[i];
		}

		return sum;
	}



	// Angle
	template <class Empty = void> requires (Size == 2)
		DataType angle() const {

		return (DataType)std::atan2(this->y, this->x);
	}

	// Yaw, towards positive x
	template <class Empty = void> requires (Size == 3)
		DataType yaw() const {

		return (DataType)std::atan2(this->z, this->x);
	}
	// Pitch, towards positive x
	template <class Empty = void> requires (Size == 3)
		DataType pitch() const {

		return (DataType)std::atan2(this->y, (DataType)std::sqrt(this->x * this->x + this->z * this->z));
	}

	// Up direction
	template <class Empty = void> requires (Size == 3)
		static Vector Up() {

		return Vector(0, 1, 0);
	}
	// Down direction
	template <class Empty = void> requires (Size == 3)
		static Vector Down() {

		return Vector(0, -1, 0);
	}
	// Left direction
	template <class Empty = void> requires (Size == 3)
		static Vector Left() {

		return Vector(-1, 0, 0);
	}
	// Right direction
	template <class Empty = void> requires (Size == 3)
		static Vector Right() {

		return Vector(1, 0, 0);
	}
	// Front direction
	template <class Empty = void> requires (Size == 3)
		static Vector Front() {

		return Vector(0, 0, 1);
	}
	// Back direction
	template <class Empty = void> requires (Size == 3)
		static Vector Back() {

		return Vector(0, 0, -1);
	}
};





// General case

template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size> operator + (const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	Vector<DataType, Size> result;
	for (int i = 0; i < Size; i++) {
		result.data[i] = vector1.data[i] + vector2.data[i];
	}

	return result;
}
template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size> operator + (const Vector<DataType, Size>& vector, DataType value) {

	Vector<DataType, Size> result;
	for (int i = 0; i < Size; i++) {
		result.data[i] = vector.data[i] + value;
	}

	return result;
}
template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size>& operator += (Vector<DataType, Size>& vector1, Vector<DataType, Size>& vector2) {

	for (int i = 0; i < Size; i++) {
		vector1.data[i] += vector2.data[i];
	}

	return vector1;
}
template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size>& operator += (Vector<DataType, Size>& vector, DataType value) {

	for (int i = 0; i < Size; i++) {
		vector.data[i] += value;
	}

	return vector;
}

template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size> operator - (const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	Vector<DataType, Size> result;
	for (int i = 0; i < Size; i++) {
		result.data[i] = vector1.data[i] - vector2.data[i];
	}

	return result;
}
template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size> operator - (const Vector<DataType, Size>& vector, DataType value) {

	Vector<DataType, Size> result;
	for (int i = 0; i < Size; i++) {
		result.data[i] = vector.data[i] - value;
	}

	return result;
}
template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size>& operator -= (Vector<DataType, Size>& vector1, Vector<DataType, Size>& vector2) {

	for (int i = 0; i < Size; i++) {
		vector1.data[i] -= vector2.data[i];
	}

	return vector1;
}
template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size>& operator -= (Vector<DataType, Size>& vector, DataType value) {

	for (int i = 0; i < Size; i++) {
		vector.data[i] -= value;
	}

	return vector;
}

template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size> operator * (const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	Vector<DataType, Size> result;
	for (int i = 0; i < Size; i++) {
		result.data[i] = vector1.data[i] * vector2.data[i];
	}

	return result;
}
template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size> operator * (const Vector<DataType, Size>& vector, DataType value) {

	Vector<DataType, Size> result;
	for (int i = 0; i < Size; i++) {
		result.data[i] = vector.data[i] * value;
	}

	return result;
}
template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size>& operator *= (Vector<DataType, Size>& vector1, Vector<DataType, Size>& vector2) {

	for (int i = 0; i < Size; i++) {
		vector1.data[i] *= vector2.data[i];
	}

	return vector1;
}
template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size>& operator *= (Vector<DataType, Size>& vector, DataType value) {

	for (int i = 0; i < Size; i++) {
		vector.data[i] *= value;
	}

	return vector;
}

template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size> operator / (const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	Vector<DataType, Size> result;
	for (int i = 0; i < Size; i++) {
		result.data[i] = vector1.data[i] / vector2.data[i];
	}

	return result;
}
template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size> operator / (const Vector<DataType, Size>& vector, DataType value) {

	Vector<DataType, Size> result;
	for (int i = 0; i < Size; i++) {
		result.data[i] = vector.data[i] / value;
	}

	return result;
}
template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size>& operator /= (Vector<DataType, Size>& vector1, Vector<DataType, Size>& vector2) {

	for (int i = 0; i < Size; i++) {
		vector1.data[i] /= vector2.data[i];
	}

	return vector1;
}
template <class DataType, int Size> requires (!OptimizeAny<DataType>::value)
Vector<DataType, Size>& operator /= (Vector<DataType, Size>& vector, DataType value) {

	for (int i = 0; i < Size; i++) {
		vector.data[i] /= value;
	}

	return vector;
}



#ifdef __AVX2__

template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator + (const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	Vector<DataType, Size> result;

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&result.data[index], _mm256_add_ps(_mm256_loadu_ps(&vector1.data[index]), _mm256_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_add_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector1.data[index] + vector2.data[index];
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator + (const Vector<DataType, Size>& vector, const DataType value) {

	Vector<DataType, Size> result;

	const __m128 _value4f = _mm_set1_ps(value);
	const __m256 _value8f = _mm256_set1_ps(value);

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&result.data[index], _mm256_add_ps(_mm256_loadu_ps(&vector.data[index]), _value8f));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_add_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector.data[index] + value;
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator += (Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&vector1.data[index], _mm256_add_ps(_mm256_loadu_ps(&vector1.data[index]), _mm256_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector1.data[index], _mm_add_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector1.data[index] += vector2.data[index];
	}

	return vector1;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator += (Vector<DataType, Size>& vector, DataType value) {

	const __m128 _value4f = _mm_set1_ps(value);
	const __m256 _value8f = _mm256_set1_ps(value);

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&vector.data[index], _mm256_add_ps(_mm256_loadu_ps(&vector.data[index]), _value8f));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector.data[index], _mm_add_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector.data[index] += value;
	}

	return vector;
}

#else

template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator + (const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	Vector<DataType, Size> result;

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_add_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector1.data[index] + vector2.data[index];
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator + (const Vector<DataType, Size>& vector, const DataType value) {

	Vector<DataType, Size> result;

	const __m128 _value4f = _mm_set1_ps(value);

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_add_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector.data[index] + value;
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator += (Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector1.data[index], _mm_add_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector1.data[index] += vector2.data[index];
	}

	return vector1;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator += (Vector<DataType, Size>& vector, const DataType value) {

	const __m128 _value4f = _mm_set1_ps(value);

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector.data[index], _mm_add_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector.data[index] += value;
	}

	return vector;
}

#endif 


#ifdef __AVX2__

template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator - (const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	Vector<DataType, Size> result;

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&result.data[index], _mm256_sub_ps(_mm256_loadu_ps(&vector1.data[index]), _mm256_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_sub_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector1.data[index] - vector2.data[index];
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator - (const Vector<DataType, Size>& vector, const DataType value) {

	Vector<DataType, Size> result;

	const __m128 _value4f = _mm_set1_ps(value);
	const __m256 _value8f = _mm256_set1_ps(value);

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&result.data[index], _mm256_sub_ps(_mm256_loadu_ps(&vector.data[index]), _value8f));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_sub_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector.data[index] - value;
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator -= (Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&vector1.data[index], _mm256_sub_ps(_mm256_loadu_ps(&vector1.data[index]), _mm256_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector1.data[index], _mm_sub_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector1.data[index] -= vector2.data[index];
	}

	return vector1;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator -= (Vector<DataType, Size>& vector, DataType value) {

	const __m128 _value4f = _mm_set1_ps(value);
	const __m256 _value8f = _mm256_set1_ps(value);

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&vector.data[index], _mm256_sub_ps(_mm256_loadu_ps(&vector.data[index]), _value8f));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector.data[index], _mm_sub_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector.data[index] -= value;
	}

	return vector;
}

#else

template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator - (const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	Vector<DataType, Size> result;

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_sub_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector1.data[index] - vector2.data[index];
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator - (const Vector<DataType, Size>& vector, const DataType value) {

	Vector<DataType, Size> result;

	const __m128 _value4f = _mm_set1_ps(value);

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_sub_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector.data[index] - value;
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator -= (Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector1.data[index], _mm_sub_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector1.data[index] -= vector2.data[index];
	}

	return vector1;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator -= (Vector<DataType, Size>& vector, const DataType value) {

	const __m128 _value4f = _mm_set1_ps(value);

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector.data[index], _mm_sub_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector.data[index] -= value;
	}

	return vector;
}

#endif 


#ifdef __AVX2__

template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator * (const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	Vector<DataType, Size> result;

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&result.data[index], _mm256_mul_ps(_mm256_loadu_ps(&vector1.data[index]), _mm256_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_mul_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector1.data[index] * vector2.data[index];
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator * (const Vector<DataType, Size>& vector, const DataType value) {

	Vector<DataType, Size> result;

	const __m128 _value4f = _mm_set1_ps(value);
	const __m256 _value8f = _mm256_set1_ps(value);

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&result.data[index], _mm256_mul_ps(_mm256_loadu_ps(&vector.data[index]), _value8f));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_mul_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector.data[index] * value;
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator *= (Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&vector1.data[index], _mm256_mul_ps(_mm256_loadu_ps(&vector1.data[index]), _mm256_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector1.data[index], _mm_mul_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector1.data[index] *= vector2.data[index];
	}

	return vector1;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator *= (Vector<DataType, Size>& vector, DataType value) {

	const __m128 _value4f = _mm_set1_ps(value);
	const __m256 _value8f = _mm256_set1_ps(value);

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&vector.data[index], _mm256_mul_ps(_mm256_loadu_ps(&vector.data[index]), _value8f));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector.data[index], _mm_mul_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector.data[index] *= value;
	}

	return vector;
}

#else

template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator * (const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	Vector<DataType, Size> result;

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_mul_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector1.data[index] * vector2.data[index];
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator * (const Vector<DataType, Size>& vector, const DataType value) {

	Vector<DataType, Size> result;

	const __m128 _value4f = _mm_set1_ps(value);

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_mul_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector.data[index] * value;
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator *= (Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector1.data[index], _mm_mul_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector1.data[index] *= vector2.data[index];
	}

	return vector1;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator *= (Vector<DataType, Size>& vector, const DataType value) {

	const __m128 _value4f = _mm_set1_ps(value);

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector.data[index], _mm_mul_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector.data[index] *= value;
	}

	return vector;
}

#endif 


#ifdef __AVX2__

template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator / (const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	Vector<DataType, Size> result;

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&result.data[index], _mm256_div_ps(_mm256_loadu_ps(&vector1.data[index]), _mm256_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_div_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector1.data[index] / vector2.data[index];
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator / (const Vector<DataType, Size>& vector, const DataType value) {

	Vector<DataType, Size> result;

	const __m128 _value4f = _mm_set1_ps(value);
	const __m256 _value8f = _mm256_set1_ps(value);

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&result.data[index], _mm256_div_ps(_mm256_loadu_ps(&vector.data[index]), _value8f));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_div_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector.data[index] / value;
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator /= (Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&vector1.data[index], _mm256_div_ps(_mm256_loadu_ps(&vector1.data[index]), _mm256_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector1.data[index], _mm_div_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector1.data[index] /= vector2.data[index];
	}

	return vector1;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator /= (Vector<DataType, Size>& vector, DataType value) {

	const __m128 _value4f = _mm_set1_ps(value);
	const __m256 _value8f = _mm256_set1_ps(value);

	constexpr int offset8f = 0;
	constexpr int count8f = (Size - offset8f) / 8;
	constexpr int offset4f = count8f * 8;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count8f; i++) {
		const int index = offset8f + i * 8;
		_mm256_storeu_ps(&vector.data[index], _mm256_div_ps(_mm256_loadu_ps(&vector.data[index]), _value8f));
	}
	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector.data[index], _mm_div_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector.data[index] /= value;
	}

	return vector;
}

#else

template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator / (const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	Vector<DataType, Size> result;

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_div_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector1.data[index] / vector2.data[index];
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size> operator / (const Vector<DataType, Size>& vector, const DataType value) {

	Vector<DataType, Size> result;

	const __m128 _value4f = _mm_set1_ps(value);

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&result.data[index], _mm_div_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		result.data[index] = vector.data[index] / value;
	}

	return result;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator /= (Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector1.data[index], _mm_div_ps(_mm_loadu_ps(&vector1.data[index]), _mm_loadu_ps(&vector2.data[index])));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector1.data[index] /= vector2.data[index];
	}

	return vector1;
}
template <class DataType, int Size> requires (OptimizeFloat<DataType>::value)
Vector<DataType, Size>& operator /= (Vector<DataType, Size>& vector, const DataType value) {

	const __m128 _value4f = _mm_set1_ps(value);

	constexpr int offset4f = 0;
	constexpr int count4f = (Size - offset4f) / 4;
	constexpr int offset1f = offset4f + count4f * 4;
	constexpr int count1f = (Size - offset1f) / 1;

	for (int i = 0; i < count4f; i++) {
		const int index = offset4f + i * 4;
		_mm_storeu_ps(&vector.data[index], _mm_div_ps(_mm_loadu_ps(&vector.data[index]), _value4f));
	}
	for (int i = 0; i < count1f; i++) {
		const int index = offset1f + i * 1;
		vector.data[index] /= value;
	}

	return vector;
}

#endif 






// These use the others and are same everywhere

template <class DataType, int Size>
Vector<DataType, Size> operator + (DataType value, const Vector<DataType, Size>& vector) {

	return Vector<DataType, Size>(value) + vector;
}
template <class DataType, int Size>
Vector<DataType, Size> operator - (DataType value, const Vector<DataType, Size>& vector) {

	return Vector<DataType, Size>(value) - vector;
}
template <class DataType, int Size>
Vector<DataType, Size> operator * (DataType value, const Vector<DataType, Size>& vector) {

	return Vector<DataType, Size>(value) * vector;
}
template <class DataType, int Size>
Vector<DataType, Size> operator / (DataType value, const Vector<DataType, Size>& vector) {

	return Vector<DataType, Size>(value) / vector;
}



template <class DataType, int Size>
Vector<DataType, Size> operator-(const Vector<DataType, Size>& vector) {

	return vector * (DataType)(-1);
}

template <class DataType, int Size>
bool operator==(const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	int equals = 0;
	for (int i = 0; i < Size; i++) {
		equals += vector1.data[i] == vector2.data[i] ? 1 : 0;
	}

	return equals == Size;
}
template <class DataType, int Size>
bool operator!=(const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

	return !(vector1 == vector2);
}



typedef class Vector<int, 2> Vector2i;
typedef class Vector<int, 3> Vector3i;
typedef class Vector<int, 4> Vector4i;

typedef class Vector<int64_t, 2> Vector2l;
typedef class Vector<int64_t, 3> Vector3l;
typedef class Vector<int64_t, 4> Vector4l;

typedef class Vector<float, 2> Vector2f;
typedef class Vector<float, 3> Vector3f;
typedef class Vector<float, 4> Vector4f;

typedef class Vector<double, 2> Vector2d;
typedef class Vector<double, 3> Vector3d;
typedef class Vector<double, 4> Vector4d;



namespace Math {

	// Normalize a vector
	template <class DataType, int Size>
	Vector<DataType, Size> Normalize(const Vector<DataType, Size>& vector) {

		return vector / vector.magnitude();
	}

	// Dot product of two vectors
	template <class DataType, int Size>
	DataType Dot(const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2) {

		DataType sum = 0;
		for (int i = 0; i < Size; i++) {
			sum += vector1.data[i] * vector2.data[i];
		}

		return sum;
	}

	// Create vector from a polar 2D coordinate system
	template <class DataType>
	Vector<DataType, 2> FromPolar(DataType length, DataType angle) {

		Vector<DataType, 2> result;
		result.data[0] = length * (DataType)std::cos(angle);
		result.data[1] = length * (DataType)std::sin(angle);

		return result;
	}

	// Create vector from a polar 3D coordinate system
	template <class DataType>
	Vector<DataType, 3> FromPolar(DataType length, DataType yaw, DataType pitch) {

		Vector<DataType, 3> result;
		result.data[0] = (DataType)std::cos(yaw) * (DataType)std::sin(pitch);
		result.data[1] = (DataType)std::sin(pitch);
		result.data[2] = (DataType)std::sin(yaw) * (DataType)std::sin(pitch);

		return result;
	}

	// Cross product of two 3D vectors
	template <class DataType>
	Vector<DataType, 3> Cross(const Vector<DataType, 3>& vector1, const Vector<DataType, 3>& vector2) {

		Vector<DataType, 3> result;
		result.data[0] = vector1.y * vector2.z - vector1.z * vector2.y;
		result.data[1] = -(vector1.x * vector2.z - vector1.z * vector2.x);
		result.data[2] = vector1.x * vector2.y - vector1.y * vector2.x;

		return result;
	}

	// Rotate a vector around an axis by angle
	template <class DataType>
	Vector<DataType, 3> Rotate(const Vector<DataType, 3>& vector, const Vector<DataType, 3>& axis, DataType angle) {

		Vector<DataType, 3> part1 = vector * (DataType)std::cos(angle);
		Vector<DataType, 3> part2 = Math::Cross<DataType>(axis, vector) * (DataType)std::sin(angle);
		Vector<DataType, 3> part3 = axis * Math::Dot(axis, vector) * ((DataType)1.0 - (DataType)std::cos(angle));

		return part1 + part2 + part3;
	}

	// Apply a function to a set of vectors
	template <class DataType, int Size, class... VectorDataType>
	Vector<DataType, Size> Apply(DataType(*function)(DataType value, VectorDataType... values), const Vector<DataType, Size>& vector, const Vector<VectorDataType, Size>&... vecs) {

		Vector<DataType, Size> result;
		for (int i = 0; i < Size; i++) {
			result.data[i] = function(vector.data[i], vecs.data[i]...);
		}

		return result;
	}

	// Apply a function to a set of vectors
	template <class DataType, int Size, class... VectorDataType>
	Vector<DataType, Size> Apply(auto&& function, const Vector<DataType, Size>& vector, const Vector<VectorDataType, Size>&... vecs) {

		Vector<DataType, Size> result;
		for (int i = 0; i < Size; i++) {
			result.data[i] = function(vector.data[i], vecs.data[i]...);
		}

		return result;
	}

	// Reduce a vector by using a function on its members
	template <class DataType, int Size>
	DataType Reduce(const Vector<DataType, Size>& vector, DataType(*function)(DataType value1, DataType value2)) {

		DataType result = function(vector.data[0], vector.data[1]);
		for (int i = 2; i < Size; i++) {
			result = function(result, vector.data[i]);
		}

		return result;
	}

	// Reduce a vector by using a function on its members
	template <class DataType, int Size>
	DataType Reduce(const Vector<DataType, Size>& vector, auto&& function) {

		DataType result = function(vector.data[0], vector.data[1]);
		for (int i = 2; i < Size; i++) {
			result = function(result, vector.data[i]);
		}

		return result;
	}

	// Interpolate between two vectors based on parameter
	template <class DataType, int Size>
	Vector<DataType, Size> Interpolate(const Vector<DataType, Size>& vector1, const Vector<DataType, Size>& vector2, float amount) {

		Vector<DataType, Size> diff = vector2 - vector1;

		return vector1 + diff * amount;
	}
}





