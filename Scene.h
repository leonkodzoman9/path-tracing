#pragma once

#include "Includes.h"

#include "Vector.h"

#include "Hittable.h"



struct Ray;



class Scene {

public:

	Vector3f getColor(const Ray& ray, int bounces) const;

	void addSphere(Sphere sphere);
	void addTriangle(Triangle triangle);
	void addQuad(Vector3f p1, Vector3f p2, Vector3f p3, Vector3f p4, Material* material);

private:

	std::vector<Sphere> spheres;
	std::vector<Triangle> triangles;

	bool getHitPoint(const Ray& ray, HitPoint& hitPoint) const;
};
